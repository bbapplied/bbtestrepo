package com.bbapplied.bbtest.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.By;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import groovy.json.StringEscapeUtils;

@DefaultUrl("http://localhost:8080")
public class BbTestPage extends PageObject 
{
	@FindBy(css="body")
    WebElement msgElement;
	//body > h2

    public void LoadPage(String baseUrl) 
    {
    	getDriver().get(baseUrl);
    }	
	
    public void Navigate(String path) 
    {
    	String url = getDriver().getCurrentUrl() + path;
    	getDriver().navigate().to(url);
    }	
	
    public void VerifyMessage(String msg) 
    {
    	String theMessage = msgElement.getText();
    	
    	new WebDriverWait(getDriver(),10).until(ExpectedConditions.textToBePresentInElement(msgElement, msg));
    }	
	
	
	
/*    //We need to take the HTML response & the Cookie (JSESSIONID) from the originating POST Call and init the WebDriver
    public void InitPage(String partner, String pageHtml, org.openqa.selenium.Cookie seleniumCookie) 
    {
    	//Open the https://qaprod.pnpit.com/r/ADGdemo as normal...
    	getDriver().get("https://qaprod.pnpit.com/r/" + partner);

    	//Set it's cookie (re-created from the )
    	getDriver().manage().addCookie(seleniumCookie);

    	//Now replace the entire HTML w/ the HTML response from the initial POST call
    	WebElement e = getDriver().findElement(By.tagName("html"));
    	String script = "arguments[0].innerHTML='" 
    	                + StringEscapeUtils.escapeJavaScript(pageHtml) + "'";
    	((JavascriptExecutor) getDriver()).executeScript(script, e);
    	
    	//The above will re-load the page, simply wait for it to load
    	new WebDriverWait(getDriver(),10).until(ExpectedConditions.titleContains(partner));
    }

    public void EnterCreditCardInfo(boolean useValidCc)
    {
    	//Click the 'Next' button...
    	getDriver().findElement(By.cssSelector("#navButtons > div > div:nth-child(3) > input")).click();
    	
    	//Click the 'Credit or Debit Card' radio button...
    	getDriver().findElement(By.id("paymentDevice1")).click();
    	
    	//Populate the Credit Card info
    	getDriver().findElement(By.cssSelector("#creditCardName")).sendKeys("Bob Flanigan"); //Name
    	getDriver().findElement(By.cssSelector("#creditCardCvv")).sendKeys("123"); //Name
    	
    	//Drop-Downs for CC Month & Year...
        WebElement monthDropDown = getDriver().findElement(By.cssSelector("#creditCardExpMonth"));
        Select select = new Select(monthDropDown);
    	select.selectByVisibleText("12");

        WebElement yearDropDown = getDriver().findElement(By.cssSelector("#creditCardExpYear"));
        select = new Select(yearDropDown);
    	select.selectByVisibleText("2030");
    	
    	//...And the CVV code, we also use this field to force a 'Decline' for CC payments...
    	if(useValidCc)
        	getDriver().findElement(By.cssSelector("#creditCardNum")).sendKeys("4111111111111111"); //Card Number
    	else
        	getDriver().findElement(By.cssSelector("#creditCardNum")).sendKeys("4003000123456781"); //Card Number

    	//Click the 'Next' button...
    	getDriver().findElement(By.name("paymentReview")).click();

    	//Wait for the 'Review Details' page...
        WebElement reviewHeaderElement = getDriver().findElement(By.cssSelector("#payment > header > h2"));
    	new WebDriverWait(getDriver(),10).until(ExpectedConditions.textToBePresentInElement(reviewHeaderElement, "Review your payment details"));
    
    	//Click the 'I Agree' check box...
    	getDriver().findElement(By.name("terms")).click();
    	
    	//..and the 'Submit' button...
    	getDriver().findElement(By.cssSelector("#submitRef")).click();
    }
    
    public void EnterECheckInfo(boolean useValidEcheck)
    {
    	//Click the 'Next' button...
    	getDriver().findElement(By.cssSelector("#navButtons > div > div:nth-child(3) > input")).click();
    	
    	//Click the 'Credit or Debit Card' radio button...
    	getDriver().findElement(By.id("paymentDevice2")).click();
    	
    	//Checking / Savings drop down...
        WebElement checkingOrSavingsDropDown = getDriver().findElement(By.name("chkaccnttype"));
        Select select = new Select(checkingOrSavingsDropDown);
    	select.selectByVisibleText("Checking");

    	//...And the CVV code, we also use this field to force a 'Decline' for CC payments...
    	if(useValidEcheck)
        	getDriver().findElement(By.name("chkroutingnum")).sendKeys("125000024"); //Routing Number
    	else
        	getDriver().findElement(By.name("chkroutingnum")).sendKeys("255077477"); //Routing Number

    	//Populate the Credit Card info
    	getDriver().findElement(By.name("chkaccntnum")).sendKeys("12345678"); //Account Number
    	getDriver().findElement(By.name("chkaccntnum2")).sendKeys("12345678"); //Verify Account Number
    	
    	//Click the 'Next' button...
    	getDriver().findElement(By.name("paymentReview")).click();

    	//Wait for the 'Review Details' page...
        WebElement reviewHeaderElement = getDriver().findElement(By.cssSelector("#payment > header > h2"));
    	new WebDriverWait(getDriver(),10).until(ExpectedConditions.textToBePresentInElement(reviewHeaderElement, "Review your payment details"));
    
    	//Click the 'I Agree' check box...
    	getDriver().findElement(By.name("terms")).click();
    	
    	//..and the 'Submit' button...
    	getDriver().findElement(By.cssSelector("#submitRef")).click();
    }

    public void VerifySuccess()
    {
    	//Wait for element to become visible
        WebElement successElement = getDriver().findElement(By.cssSelector("body > main > header > h2:nth-child(1)"));
    	new WebDriverWait(getDriver(),10).until(ExpectedConditions.presenceOfElementLocated(net.serenitybdd.core.annotations.findby.By.cssSelector("body > main > header > h2:nth-child(1)")));
    	//new WebDriverWait(getDriver(),10).until(ExpectedConditions.visibilityOf(successElement));

    	//Validate...Wait for 'Success' message...
    	new WebDriverWait(getDriver(),10).until(ExpectedConditions.textToBePresentInElement(successElement, "Success!"));
    }
    
    public void VerifyDecline()
    {
    	//Wait for element to become visible
        WebElement failureElement = getDriver().findElement(By.cssSelector("#payment > div > div > div.col.col-md-10.col-xl-8 > header > span:nth-child(1) > p"));
    	new WebDriverWait(getDriver(),10).until(ExpectedConditions.presenceOfElementLocated(net.serenitybdd.core.annotations.findby.By.cssSelector("#payment > div > div > div.col.col-md-10.col-xl-8 > header > span:nth-child(1) > p")));
    	//new WebDriverWait(getDriver(),10).until(ExpectedConditions.visibilityOf(failureElement));
    	
    	//Validate...Wait for 'Failure' message...
    	new WebDriverWait(getDriver(),10).until(ExpectedConditions.textToBePresentInElement(failureElement, "Your Bank did not authorize this transaction. Please validate payment information or use a new payment device."));
    }
  */  
}
