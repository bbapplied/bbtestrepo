package com.bbapplied.bbtest.mappings;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Map;

import org.hamcrest.Matchers;

import com.bbapplied.bbtest.World;
import com.bbapplied.bbtest.steps.BbTestSteps;
import com.pointandpay.resource.ClassResource;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.reports.WithTitle;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

public class BbTest extends World
{
	@Steps BbTestSteps steps;

	//private Response response;
	//private ValidatableResponse data;
	//private RequestSpecification request;

	@Before
	public void setup() 
	{
		//RestAssured.baseURI = "http://localhost:8080/bbapp";
		//RestAssured.port = 443;
		//RestAssured.defaultParser = Parser.XML;
	}

	//*************************************************
	public org.openqa.selenium.Cookie Helper_CreateCookieForWebDriver() 
	{
		org.openqa.selenium.Cookie ret = null;

		//We need to copy the Cookie and any Session info so we can re-use it with a WebDriver
		Cookies cookies = SerenityRest.then().extract().detailedCookies();

		//Ultimately we need the JSESSIONID from the cookie, re-create a cookie that the WebDriver can use for the PageObjects to consume
		for (Cookie ck : cookies)  
		{
			ret = new org.openqa.selenium.Cookie(ck.getName(), ck.getValue(), ck.getDomain(), ck.getPath(), ck.getExpiryDate(), ck.isSecured(), ck.isHttpOnly());
		}

		//Assert!!!
		assertNotNull(ret);
		
		return ret;
	}

	@Given("I my base address is {string}")
	public void i_my_base_address_is(String string) 
	{
		steps.Init(string);
	}
	
	@When("I navigate to {string}")
	public void i_navigate_to(String string) 
	{
		steps.NavigateTo(string);
	}

	@Then("I see the message {string}")
	public void i_see_the_message(String string) 
	{
		steps.VerifyMessage(string);
	}

	
	
	/*@Given("I Parameter Pass one item to Partner {string}")
	public void i_Parameter_Pass_one_item_to_Partner(String string) 
	{
		//Set the BaseUri to the partner specific site...
		RestAssured.baseURI += string;
		
		//You need to specify the parameter as queryParameter and not "param" or "parameter". Param for POST will default to form parameters which are sent in the request body.
		request = SerenityRest.given().queryParam("paramXML", super.helper.CreateXml());

		//Send the POST to initiate the sequence
		response = SerenityRest.when().post();

		//Get the cookie that the WebDriver will use to re-connect to the partner page
		org.openqa.selenium.Cookie cookie = Helper_CreateCookieForWebDriver();

		//Now continue by using the Step Library methods
		steps.InitializeWebDriver(string, response.getBody().print(), cookie);
	}

	@Given("I Parameter Pass two items to Partner {string}")
	public void i_Parameter_Pass_two_items_to_Partner(String string) 
	{
		//Set the BaseUri to the partner specific site...
		RestAssured.baseURI += string;
		
		//You need to specify the parameter as queryParameter and not "param" or "parameter". Param for POST will default to form parameters which are sent in the request body.
		request = SerenityRest.given().queryParam("paramXML", super.helper_2.CreateXml());

		//Send the POST to initiate the sequence
		response = SerenityRest.when().post();

		//Get the cookie that the WebDriver will use to re-connect to the partner page
		org.openqa.selenium.Cookie cookie = Helper_CreateCookieForWebDriver();

		//Now continue by using the Step Library methods
		steps.InitializeWebDriver(string, response.getBody().print(), cookie);
	}

	@When("I submit valid Credit Card information")
	public void i_submit_valid_Credit_Card_information() 
	{
		//The 'Given' statement did the POST and initialized a WebDriver instance so we can continue the automation
		steps.SubmitCreditCardInfo(true); //pass 'true' for a valid credit card
	}

	@When("I submit valid Echeck information")
	public void i_submit_valid_Echeck_information() 
	{
		steps.SubmitEcheckInfo(true); //pass 'true' for a valid credit card
	}

	@When("I submit invalid Echeck information")
	public void i_submit_invalid_Echeck_information() 
	{
		steps.SubmitEcheckInfo(false); //pass 'true' for a valid credit card
	}

	@When("I submit invalid Credit Card information")
	public void i_submit_invalid_Credit_Card_information() 
	{
		//The 'Given' statement did the POST and initialized a WebDriver instance so we can continue the automation
		steps.SubmitCreditCardInfo(false); //pass 'true' for a valid credit card
	}

	@Then("I successfully process the payment")
	public void i_successfully_process_the_payment() 
	{
		steps.VerifySuccess();
	}

	@Then("the payment is declined")
	public void the_payment_is_declined() 
	{
		steps.VerifyDecline();
	}
*/



	
}
