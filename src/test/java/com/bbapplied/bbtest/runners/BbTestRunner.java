package com.bbapplied.bbtest.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;

import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
//@RunWith(SerenityRunner.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/cucumber-html-report"},
    	glue = {"com.bbapplied.bbtest.mappings"},
		features = {"classpath:features"})
public class BbTestRunner 
{
}
