package com.bbapplied.bbtest;

import java.util.Map;

import com.bbapplied.bbtest.helpers.XmlHelper;
import com.pointandpay.resource.ClassResource;

//bb, this is ugly but to pass values between 'stepdefs' have each 'extend' this class
public class World 
{
	public static XmlHelper helper = new XmlHelper(); 
	public static XmlHelper helper_2 = new XmlHelper(); 
	//public static Map<String, String> cookies;
	
	public World()
	{
		//********************************************************************
		//Initialize the XmlHelper with template data to pass to API3 services
		String baseXml = ClassResource.load(World.class, "templateUrlpp.xml");
		helper.Parse(baseXml);

		String baseXml_2 = ClassResource.load(World.class, "templateUrlpp_2.xml");
		helper_2.Parse(baseXml_2);
	}
	
}
