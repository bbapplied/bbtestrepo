package com.bbapplied.bbtest.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertFalse;

import org.hamcrest.Matchers;

import com.bbapplied.bbtest.pages.BbTestPage;
import com.pointandpay.resource.ClassResource;

//import io.restassured.RestAssured;
//import io.restassured.parsing.Parser;
//import io.restassured.response.Response;
//import io.restassured.response.ValidatableResponse;
//import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.reports.WithTitle;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class BbTestSteps 
{
	BbTestPage page;                                          

	//*******************************************************************
	@Step                                                       
    public void Init(String baseUrl) 
    {
		page.LoadPage(baseUrl);
    }
	
	@Step
	public void NavigateTo(String path)
	{
		page.Navigate(path);
	}
	
	@Step
	public void VerifyMessage(String msg)
	{
		page.VerifyMessage(msg);
	}
}
