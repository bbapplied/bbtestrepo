package com.bbapplied.bbtest.helpers;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class XmlHelper 
{
	Document doc;
	Element root;

	public XmlHelper()
	{
	
	}
	
	public void Parse(String xmlContent)
	{
		//Create DocumentBuilder...
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			//Create Document from the xmlContent passed to us...
			StringBuilder xmlStringBuilder = new StringBuilder();
			xmlStringBuilder.append(xmlContent);
			ByteArrayInputStream input = new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
			
			//Now that the input is in the correct format, Parse in the data...
			doc = builder.parse(input);
			
			//Extract the 'root' element and store in member var...
			root = doc.getDocumentElement();
		}
		catch(Exception ex)
		{
		}
	}
	
	public String GetElementText(String elementName)
	{
		String ret = "";
		
		try
		{
			//Search for the Element by Name...
			NodeList nList = doc.getElementsByTagName(elementName);
			//Cast to an Element object...
			Element eElement = (Element)nList.item(0);
			//Get the value of that element... 
			ret = eElement.getTextContent();
		}
		catch(Exception ex)
		{
		}

		return ret;
	}
	
	public void SetElementText(Integer indexOf, String elementName, String newElementText)
	{
		try
		{
			//Search for the Element by Name...
			NodeList nList = doc.getElementsByTagName(elementName);
			//Cast to an Element object...
			Element eElement = (Element)nList.item(indexOf);
			//Get the value of that element... 
			eElement.setTextContent(newElementText);
		}
		catch(Exception ex)
		{
		}
	}
	
	public String CreateXml()
	{
		String ret = "";
		
		TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try 
        {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            
            //Convert the DOM to a String
            ret = writer.getBuffer().toString();
        } 
        catch (TransformerException e) 
        {
            e.printStackTrace();
        }
        
        return ret;
	}

}
