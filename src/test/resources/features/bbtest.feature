Feature: bbApp Basic Functionality
  Scenario: Base URL availability
	Given I my base address is "http://localhost:8080"
	When I navigate to "/bbapp"
	Then I see the message "Hello World!"

  Scenario: Servlet availability
	Given I my base address is "http://localhost:8080/bbapp"
	When I navigate to "/hello"
	Then I see the message "Hello World! Maven Web Project Example."
	